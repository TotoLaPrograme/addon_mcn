import bpy

class MCN_PT_AP(bpy.types.Panel):
    """"""
    bl_label = "AP"
    bl_idname = "MCN_PT_AP"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        # TODO Replace this label by your stuff here
        layout.label(text="AP’s tools here")


def register():
    bpy.utils.register_class(MCN_PT_AP)


def unregister():
    bpy.utils.unregister_class(MCN_PT_AP)
